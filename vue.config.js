module.exports = {
    publicPath: './',
    devServer: {
            proxy: { //匹配规则
                '/api': {
                    //要访问的跨域的域名
                    target: 'http://192.168.1.195:8080',
                    secure:false, // 使用的是http协议则设置为false，https协议则设置为true
                    changOrigin: true, //开启代理
                    pathRewrite: {
                        '^/api': ''
                    }
                }
            }
        }
    
 
}