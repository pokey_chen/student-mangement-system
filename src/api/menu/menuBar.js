const menuData = [
  {
    index: '1',
    id: 401,
    primaryMenu: '民主投票',
    secondaryMenu: [
      {
        submenu: '投票活动管理',
        index: '/voteActiveManage'
      },
      {
        submenu: '稿件管理',
        index: '/manuscriptManage'
      },
      {
        submenu: '投票记录',
        index: '/votingRecords'
      }
    ]
  },
  {
    index: '2',
    id: 402,
    primaryMenu: '轮播图片',
    secondaryMenu: [
      {
        submenu: '轮播图片管理',
        index: '/rotaPicManage'
      }
    ]
  },
  {
    index: '3',
    id: 403,
    primaryMenu: '通知公告',
    secondaryMenu: [
      {
        submenu: '通知公告管理',
        index: '/announce'
      }
    ]
  },
  {
    index: '4',
    id: 404,
    primaryMenu: '监督执纪',
    secondaryMenu: [
      {
        submenu: '书记信箱',
        index: '/letter'
      },
      {
        submenu: '匿名举报',
        index: '/anonymous'
      }
    ]
  },
  {
    index: '5',
    id: 405,
    primaryMenu: '组织活动',
    secondaryMenu: [
      {
        submenu: '活动管理',
        index: '/activeMange'
      },
      {
        submenu: '活动报名记录',
        index: '/activeRegistrateRecord'
      },
      {
        submenu: '活动留言记录',
        index: '/activeLeaveRecord'
      }
    ]
  },
  {
    index: '6',
    id: 406,
    primaryMenu: '党建动态',
    secondaryMenu: [
      {
        submenu: '文章管理',
        index: '/articleManage'
      },
      {
        submenu: '文章分类管理',
        index: '/articleClassificateManage'
      },
      {
        submenu: '文章评论管理',
        index: '/articleCommitManage'
      }
    ]
  },
  {
    index: '7',
    id: 407,
    primaryMenu: '党员管理',
    secondaryMenu: [
      {
        submenu: '党员管理',
        index: '/partyMember'
      },
      {
        submenu: '回收站',
        index: '/recycle'
      }
    ]
  },
  {
    index: '8',
    id: 408,
    primaryMenu: '组织管理',
    secondaryMenu: [
      {
        submenu: '组织管理',
        index: '/changeTeam'
      },
      {
        submenu: '关系变更',
        index: '/organicChange'
      }
    ]
  },
  {
    index: '9',
    id: 409,
    primaryMenu: '参数配置',
    secondaryMenu: [
      {
        submenu: '参数配置',
        index: '/systemInformation'
      }  
    ]
  },
  {
      index: '10',
      id: 410,
      primaryMenu: '党员学习',
      secondaryMenu: [
        {
          submenu: '课程管理',
          index: '/courseManagement'
        },
        {
          submenu: '分类管理',
          index: '/classifiedManagement'
        },
        {
          submenu: '章节管理',
          index: '/chapterDesign'
        },
        {
          submenu: '课程记录',
          index: '/courseRecord'
        },
        {
          submenu: '章节记录',
          index: '/learningRecord'
        }
      ]
    },
  {
      index: '11',
      id: 411,
      primaryMenu: '志愿服务',
      secondaryMenu: [
        {
          submenu: '分类管理',
          index: '/classificationManagement'
        },
        {
          submenu: '项目管理',
          index: '/projectManagement'
        },
        {
          submenu: '服务认领',
          index: '/serviceClaim'
        },
        {
          submenu: '留言记录',
          index: '/messageRecord'
        }
      ]
    },
  {
    index: '12',
    id: 412,
    primaryMenu: '党员论坛',
    secondaryMenu: [
      {
        submenu: '话题管理',
        index: '/topicManagement'
      },
      {
        submenu: '分类管理',
        index: '/topicClassManagement'
      },
      {
        submenu: '点赞管理',
        index: '/likeManagement'
      },
      {
        submenu: '回复管理',
        index: '/replyManagement'
      },
      {
        submenu: '收藏管理',
        index: '/collectionManagement'
      }
    ]
  },
  {
    index: '13',
    id: 413,
    primaryMenu: '管理人员',
    secondaryMenu: [
      {
        submenu: '管理人员',
        index: '/changeMan'
      }
    ]
  },
  {
    index: '14',
    id: 414,
    primaryMenu: '红色阵地',
    secondaryMenu: [
      {
        submenu: '红色阵地',
        index: '/acquisition'
      }
    ]
  }
]

module.exports.menuData = menuData
