/**
 * @description 导出列表
 * @param {Object} data 接口数据
 * @param {String} title 导出文件名
 * @param {String} type 导出类型, 默认为excel, 可选 "csv"
 */
import axios from 'axios'

export function exportList (data, title, type) {
  type = type || 'application/vnd.ms-excel'
  const blob = new Blob([data], {
    type: type
  })
  const fileName = `${title}${type.toUpperCase() === 'CSV' ? '.csv' : '.xls'}`
  if (window.navigator.msSaveOrOpenBlob) {
    navigator.msSaveBlob(blob, fileName)
  } else {
    const link = document.createElement('a')
    link.style = 'display:none'
    document.body.appendChild(link)
    link.href = window.URL.createObjectURL(blob)
    link.download = fileName
    link.click()
    window.URL.revokeObjectURL(link.href)
    document.body.removeChild(link)
  }
}
