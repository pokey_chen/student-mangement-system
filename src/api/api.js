/**
* api接口统一管理
*/
import {get, post} from '../axios/http.js'

export const api = {
    // 管理员登录接口
    login: (p) => post('/party/user/login', p),
    // 管理员登出接口
    logout: (p) => post('/party/user/logout', p),

    // 添加投票活动接口
    addVoteEvent: (p) => post('/party/vote/addVoteEvent', p),
    // 编辑投票活动接口
    updateVoteEvent: (p) => post('/party/vote/updateVoteEvent', p),
    // 获取投票活动接口
    getVoteEvent: (p) => post('/party/vote/getVoteEvent', p),
    // 获取投票活动下拉框：
    getVoteEventBox: (p) => post('/party/vote/getVoteEventBox', p),
    // 删除投票活动接口
    deleteVoteEvent: (p) => post('/party/vote/deleteVoteEvent', p),

    // 添加投稿件接口
    addManuscript: (p) => post('/party/vote/addManuscript', p),
    // 编辑稿件接口
    updateManuscript: (p) => post('/party/vote/updateManuscript', p),
    // 获取稿件接口
    getManuscript: (p) => post('/party/vote/getManuscript', p),
    // 稿件导出接口
    downloadManuscript: (p) => get('/party/vote/downloadManuscript', p),
    // 删除稿件接口
    deleteManuscript: (p) => post('/party/vote/deleteManuscript', p),

    // 获取投票记录接口
    getVoteRecord: (p) => post('/party/vote/getVoteRecord', p),
    // 删除投票记录接口
    deleteVoteRecord: (p) => post('/party/vote/deleteVoteRecord', p),
    // 投票记录导出接口：
    downloadVoteRecord: (p) => get('/party/vote/downloadVoteRecord', p),

    // 添加轮播图片接口：
    addBannerInfo: (p) => post('/party/banner/addBannerInfo', p),
    // 编辑轮播图片接口：
    updateBannerInfo: (p) => post('/party/banner/updateBannerInfo', p),
    // 删除轮播图片接口：
    deleteBannerInfo: (p) => post('/party/banner/deleteBannerInfo', p),
    // 获取轮播图接口：
    getBannerInfo: (p) => post('/party/banner/getBannerInfo', p),

    // 添加通知公告接口：
    addNotification: (p) => post('/party/notification/addNotification', p),
    // 编辑通知公告接口：/party/notification/updateNotification
    updateNotification: (p) => post('/party/notification/updateNotification', p),
    // 删除通知公告接口：/party/notification/deleteNotification
    deleteNotification: (p) => post('/party/notification/deleteNotification', p),
    // 获取通知公告接口：/party/notification/getNotification
    getNotification: (p) => post('/party/notification/getNotification', p),

    // 获取组织下拉框接口：
    getOrganizationBox: (p) => post('/party/organization/getOrganizationBox', p),

    // 74.	书记信箱查询接口：
    getMessageBoxItem: (p) => post('/party/supervise/getMessageBoxItem', p),
    // 75.	匿名举报查询接口：
    getAnonymousReportItem: (p) => post('/party/supervise/getAnonymousReportItem', p),
    // 73.	匿名举报 和 书记信箱 删除接口：
    deleteSupervise: (p) => post('/party/supervise/deleteSupervise', p),

    // 76.	组织活动查询接口：
    getOrganizationActivity: (p) => post('/party/organizationActivity/getOrganizationActivity', p),
    // 77.	组织活动添加接口：
    addOrganizationActivity: (p) => post('/party/organizationActivity/addOrganizationActivity', p),
    // 78.	组织活动修改接口：
    updateOrganizationActivity: (p) => post('/party/organizationActivity/updateOrganizationActivity', p),
    // 79.	组织活动删除接口：
    deleteOrganizationActivity: (p) => post('/party/organizationActivity/deleteOrganizationActivity', p),

    // 80.	组织活动报名记录查询接口：
    getOrganizationActivityApplyRecord: (p) => post('/party/organizationActivity/getOrganizationActivityApplyRecord', p),
    // 81.	组织活动报名记录导出接口：
    downloadOrganizationActivityApplyRecord: (p) => get('/party/organizationActitvity/downloadOrganizationActivityApplyRecord', p),

    // 82.	组织活动留言记录查询接口：
    getOrganizationActivityMessageRecord: (p) => post('/party/organizationActivity/getOrganizationActivityMessageRecord', p),
    // 83.	组织活动留言记录导出接口：
    downloadOrganizationActivityMessageRecord: (p) => post('/party/organizationActivity/downloadOrganizationActivityMessageRecord', p),

    // 97.	文章查询接口：
    getArticle: (p) => post('/party/article/getArticle', p),
    // 98.	文章添加接口：
    addArticle: (p) => post('/party/article/addArticle', p),
    // 99.	文章修改接口：
    updateArticle: (p) => post('/party/article/updateArticle', p),
    // 100.	文章删除接口：
    deleteArticle: (p) => post('/party/article/deleteArticle', p),

    // 101.	文章分类下拉框接口：
    getArticleTypeBox: (p) => post('/party/article/getArticleTypeBox', p),

    // 102.	文章分类查询接口：
    getArticleType: (p) => post('/party/article/getArticleType', p),
    // 103.	文章分类添加接口：
    addArticleType: (p) => post('/party/article/addArticleType', p),
    // 104.	文章分类修改接口：
    updateArticleType: (p) => post('/party/article/updateArticleType', p),
    // 105.	文章分类删除接口：
    deleteArticleType: (p) => post('/party/article/deleteArticleType', p),

    // 106.	文章评论查询接口：
    getArticleMessage: (p) => post('/party/article/getArticleMessage', p),
    // 107.	文章评论记录导出接口：
    downloadArticleMessageRecordFile: (p) => get('/party/article/downloadArticleMessageRecordFile', p),
    // 108.	文章评论删除接口：
    deleteArticleMessage: (p) => post('/party/article/deleteArticleMessage', p),


    // 125.	文章状态修改接口：
    updateArticleStatus: (p) => post('/party/article/updateArticleStatus', p),
    // 126.	文章评论显示或隐藏更新接口：
    updateArticleMessage: (p) => post('/party/article/updateArticleMessage', p),
    // 127.	组织活动留言显示或隐藏接口：
    updateOrganizationActivityMessageRecord: (p) => post('/party/organizationActivity/updateOrganizationActivityMessageRecord', p),


    //添加管理人员
    addUser:(p)=> post('/party/user/addUser', p),
    //	编辑管理人员
    updateUser:(p)=> post('/party/user/updateUser', p),
    //28.	删除管理人员
    deleteUser:(p)=> post('/party/user/deleteUser', p),
    //29.	获取管理人员接口
      getUser:(p)=> post('/party/user/getUser', p),
    //31.	添加党员
    addCommunist:(p)=> post('/party/communist/addCommunist', p),
    // 34.	编辑党员接口
    updateCommunist:(p)=> post('/party/communist/updateCommunist', p),
    // 35.	回收党员接口
    deleteCommunist:(p)=> post('/party/communist/deleteCommunist', p),
    //36.	获取党员接口
     getCommunist:(p)=> post('/party/communist/getCommunist', p),
    //  37.	获取被回收党员接口
    getRecycledCommunist:(p)=> post('/party/communist/getRecycledCommunist', p),
    //38.	党员下拉框接口
    getCommunistBox:(p)=> post('/party/communist/getCommunistBoxForUser', p),
    // 42.	添加组织接口
    addOrganization:(p)=> post('/party/organization/addOrganization', p),
    // 43.	编辑组织接口
    updateOrganization:(p)=> post('/party/organization/updateOrganization', p),
    // 44.	删除组织接口 
    deleteOrganization:(p)=> post('/party/organization/deleteOrganization', p),
    // 45.	获取组织接口:
    getOrganization:(p)=> post('/party/organization/getOrganization', p),
    //47.	获取组织下拉框接口
    getOrganizationBox:(p)=> post('/party/organization/getOrganizationBox', p),
    // 48.	编辑系统信息接口
    updateSystemInfo:(p)=> post('/party/system/updateSystemInfo', p),
    // 49.	获取系统信息接口
    getSystemInfo:(p)=> post('/party/system/getSystemInfo', p),
    //50.	编辑党员登录配置接口
    updateCommunistLogin:(p)=> post('/party/system/updateCommunistLogin', p),
    //51.	获取党员登录配置接口 
    getCommunistLogin:(p)=> post('/party/system/getCommunistLogin', p),
    // 52.	编辑用户个人中心接口
    updatePersonalCenter:(p)=> post('/party/system/updatePersonalCenter', p),
    //53.	获取用户个人中心接口
    getPersonalCenter:(p)=> post('/party/system/getPersonalCenter', p),
    // 54.	课程分类查询接口
    getCourseType:(p)=> post('/party/course/getCourseType', p),
    // 55.	课程分类添加接口
    addCourseType:(p)=>post('/party/course/addCourseType', p),
    // 56.	课程分类修改接口
    updateCourseType:(p)=>post('/party/course/updateCourseType', p),
    // 57.	课程分类删除接口
    deleteCourseType:(p)=>post('/party/course/deleteCourseType', p),
    // 58.	课程查询接口
    getCourse:(p)=>post('/party/course/getCourse', p),
    //59. 课程分类下拉框接口
    getCourseTypeBox:(p)=>post('/party/course/getCourseTypeBox', p),
    // 61.	课程添加接口
    addCourse:(p)=>post('/party/course/addCourse', p),
    //62.	课程修改接口
    updateCourse:(p)=>post('/party/course/updateCourse', p),
    // 63.	课程删除接口
    deleteCourse:(p)=>post('/party/course/deleteCourse', p),
    // 64.	课程下拉框接口
    getCourseBox:(p)=>post('/party/course/getCourseBox', p),
    // 65.	章节查询接口
    getChapter:(p)=>post('/party/course/getChapter', p),
    // 67.	章节添加接口
    addChapter:(p)=>post('/party/course/addChapter', p),
    // 68.	章节修改接口
    updateChapter:(p)=>post('/party/course/updateChapter', p),
    // 69.	章节删除接口
    deleteChapter:(p)=>post('/party/course/deleteChapter', p),
    // 70.	课程学习记录查询接口
    getCourseLearningRecord:(p)=>post('/party/course/getCourseLearningRecord', p),
    //72.	章节学习记录查询接口
    getChapterLearningRecord:(p)=>post('/party/course/getChapterLearningRecord', p),
    // 84.	志愿服务分类下拉框接口
    getVolunteeringTypeBox:(p)=>post('/party/volunteering/getVolunteeringTypeBox', p),
   //85.	志愿服务分类查询接口
    getVolunteeringType:(p)=>post('/party/volunteering/getVolunteeringType', p),
    // 86.	志愿服务分类添加接口
    addVolunteeringType:(p)=>post('/party/volunteering/addVolunteeringType', p),
    // 87.	志愿服务分类修改接口
    updateVolunteeringType:(p)=>post('/party/volunteering/updateVolunteeringType', p),
    // 88.	志愿服务分类删除接口
    deleteVolunteeringType:(p)=>post('/party/volunteering/deleteVolunteeringType', p),
    // 89.	志愿服务项目查询接口
    getVolunteering:(p)=>post('/party/volunteering/getVolunteering', p),
    //90.	志愿服务项目添加接口
    addVolunteering:(p)=>post('/party/volunteering/addVolunteering', p),
    // 91.	志愿服务项目修改接口
    updateVolunteering:(p)=>post('/party/volunteering/updateVolunteering', p),
    // 92.	志愿服务项目删除接口
    deleteVolunteering:(p)=>post('/party/volunteering/deleteVolunteering', p),
    // 93.	志愿服务认领记录查询接口
    getVolunteeringClaimRecord:(p)=>post('/party/volunteering/getVolunteeringClaimRecord', p),
    // 95.	志愿服务留言记录查询接口
    getVolunteeringMessageRecord:(p)=>post('/party/volunteering/getVolunteeringMessageRecord', p),
    // 109.	话题查询接口
    getTopic:(p)=>post('/party/topic/getTopic', p),
    // 110.	话题删除接口
    deleteTopic:(p)=>post('/party/topic/deleteTopic', p),
    //111.	话题分类查询接口
    getTopicType:(p)=>post('/party/topic/getTopicType', p),
    // 112.	话题分类下拉框接口
    getTopicTypeBox:(p)=>post('/party/topic/getTopicTypeBox', p),
    //113.	话题分类添加接口
    addTopicType:(p)=>post('/party/topic/addTopicType', p),
    // 114.	话题分类修改接口
   updateTopicType:(p)=>post('/party/topic/updateTopicType', p),
    // 115.	话题分类删除接口
    deleteTopicType:(p)=>post('/party/topic/deleteTopicType', p),
    // 116.	点赞记录查询接口
    getLikeItem:(p)=>post('/party/topic/getLikeItem', p),
    // 117.	点赞记录删除接口
    deleteLikeItem:(p)=>post('/party/topic/deleteLikeItem',p),
    // 118.	回复记录查询接口
    getReplyItem:(p)=>post('/party/topic/getReplyItem',p),
    //119.	回复记录删除接口
    deleteReplyItem:(p)=>post('/party/topic/deleteReplyItem',p),
    //120.	收藏记录查询接口
    getFavoritesItem:(p)=>post('/party/topic/getFavoritesItem',p),
    // 121.	收藏记录删除接口
    deleteFavoritesItem:(p)=>post('/party/topic/deleteFavoritesItem',p),
    // 122.	党员状态修改接口
    updateCommnuistStatus:(p)=>post('/party/communist/updateCommunistStatus',p),
    // 123.	课程状态修改接口
    updateCourseStatus:(p)=>post('/party/course/updateCourseStatus',p),
    //124.	章节状态修改接口
    updateChapterStatus:(p)=>post('/party/course/updateChapterStatus',p),

    //128.	红色阵地增加接口
    addRedCitadel:(p)=>post('/party/redcitadel/addRedCitadel',p),
    //129.	列表查看红色阵地：
    getRedCitadelList:(p)=>post('/party/redcitadel/getRedCitadelList',p),
    //130.	查看红色阵地详情：
    getRedCitadel:(p)=>post('/party/redcitadel/getRedCitadel',p),
    //124.	131.	删除红色阵地：
    deleteRedCitadel:(p)=>post('/party/redcitadel/deleteRedCitadel',p),
    // 125回复党员接口
    restoreCommunist:(p)=>post('/party/communist/restoreCommunist',p),
    // 135.	列表查看组织关系变动申请
    getCommunistOrganizationFlowList:(p)=>post('/party/communist/getCommunistOrganizationFlowList',p),
    // 136.	审批组织关系变动申请
    approvalCommunistOrganizationFlow:(p)=>post('/party/communist/approvalCommunistOrganizationFlow',p)
}

