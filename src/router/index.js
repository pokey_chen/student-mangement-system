import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    redirect: '/voteActiveManage',
    component: () => import('../components/Home/Home.vue'),
    children: [
      {
        path: '/manuscriptManage',
        name: 'ManuscriptManage',
        component: () => import( '../views/voting/Manuscript-manage.vue')
      },
      {
        path: '/voteActiveManage',
        name: 'VoteActiveManage',
        component: () => import('../views/voting/Vote-Active-Manage.vue')
      },
      {
        path: '/votingRecords',
        name: 'VotingRecords',
        component: () => import('../views/voting/Voting-Records.vue')
      },
      {
        path: '/rotaPicManage',
        name: 'rotationPictureManage',
        component: () => import('../views/Rotate/rotationPictureManage.vue')
      },
      {
        path: '/anonymous',
        name: 'anonymous',
        component: () => import('../views/Superive/anonymous.vue')
      },
      {
        path: '/letter',
        name: 'letter',
        component: () => import('../views/Superive/letter.vue')
      },
      {
        path: '/announce',
        name: 'announceManage',
        component: () => import('../views/Notice/announceManage.vue')
      },
      {
        path: '/activeLeaveRecord',
        name: 'activeLeaveRecord',
        component: () => import( '../views/Organizate/activeLeaveRecord.vue')
      },
      {
        path: '/activeMange',
        name: 'activeManage',
        component: () => import('../views/Organizate/activeManage.vue')
      },
      {
        path: '/activeRegistrateRecord',
        name: 'activeRegistrateRecord',
        component: () => import( '../views/Organizate/activeRegistrateRecord.vue')
      },
      {
        path: '/articleClassificateManage',
        name: 'articleClassificateManage',
        component: () => import( '../views/Buildtrends/articleClassificateManage.vue')
      },
      {
        path: '/articleCommitManage',
        name: 'articleCommitManage',
        component: () => import( '../views/Buildtrends/articleCommitManage.vue')
      },
      {
        path: '/articleManage',
        name: 'articleManage',
        component: () => import( '../views/Buildtrends/articleManage.vue')
      },
      {
        path: '/changeMan',
        name: 'changeManageMan',
        component: () =>
            import ('../views/changeManage/changeMan.vue')
    },
    {
        path: '/partyMember',
        name: 'partyMember',
        component: () =>
            import ('../views/partyMember/partyMember.vue')
    },
    {
        path: '/recycle',
        name: 'recycle',
        component: () =>
            import ('../views/partyMember/recycle.vue')
    },
    {
        path: '/changeTeam',
        name: 'changeTeam',
        component: () =>
            import ('../views/changeTeam/changeTeam.vue')
    },
    {
        path: '/systemInformation',
        name: 'systemInformation',
        component: () =>
            import ('../views/allocation/systemInformation.vue')
    },
    {
        path: '/personalCenter',
        name: 'personalCenter',
        component: () =>
            import ('../views/allocation/personalCenter.vue')
    },
    {
        path: '/courseManagement',
        name: 'courseManagement',
        component: () =>
            import ('../views/studyOfPartyMembers/courseManagement.vue')
    },
    {
        path: '/chapterDesign',
        name: 'chapterDesign',
        component: () =>
            import ('../views/studyOfPartyMembers/chapterDesign.vue')
    },
    {
        path: '/classifiedManagement',
        name: 'classifiedManagement',
        component: () =>
            import ('../views/studyOfPartyMembers/classifiedManagement.vue')
    },
    {
        path: '/courseRecord',
        name: 'courseRecord',
        component: () =>
            import ('../views/studyOfPartyMembers/courseRecord.vue')
    },
    {
        path: '/learningRecord',
        name: 'learningRecord',
        component: () =>
            import ('../views/studyOfPartyMembers/learningRecord.vue')
    },
    {
      path: '/classificationManagement',
      name: 'classificationManagement',
      component: () =>
          import ('../views/volunteerService/classificationManagement.vue')
  },
  {
    path: '/projectManagement',
    name: 'projectManagement',
    component: () =>
        import ('../views/volunteerService/projectManagement.vue')
    },
    {
      path: '/serviceClaim',
      name: 'serviceClaim',
      component: () =>
          import ('../views/volunteerService/serviceClaim.vue')
    },
    {
      path: '/messageRecord',
      name: 'messageRecord',
      component: () =>
          import ('../views/volunteerService/messageRecord.vue')
    },
    {
      path: '/topicManagement',
      name: 'topicManagement',
      component: () =>
          import ('../views/partyMemberForum/topicManagement.vue')
    },
    // {
    //   path: '/likeManagement',
    //   name: 'likeManagement',
    //   component: () =>
    //       import ('../views/partyMemberForum/likeManagement.vue')
    // },
    {
      path: '/likeManagement',
      name: 'likeManagement',
      component: () =>
          import ('../views/partyMemberForum/likeManagement.vue')
    },
    {
      path: '/replyManagement',
      name: 'replyManagement',
      component: () =>
          import ('../views/partyMemberForum/replyManagement.vue')
    },
    {
      path: '/collectionManagement',
      name: 'collectionManagement',
      component: () =>
          import ('../views/partyMemberForum/collectionManagement.vue')
    },
    {
      path: '/acquisition',
      name: 'acquisition',
      component: () =>
          import ('../views/redAcquisition/acquisition.vue')
    },
     {
       path: '/topicClassManagement',
       name: 'topicClassManagement',
       component: () =>
           import ('../views/partyMemberForum/classifiedManagement.vue')
     },
     {
      path: '/organicChange',
      name: 'organicChange',
      component: () =>
          import ('../views/changeTeam/organicChange.vue')
    }
    ]
  }
]

const router = new VueRouter({
    routes
})

 //挂载路由导航守卫
 router.beforeEach((to, from, next) => {
   // to 表示将要访问的路径，from 表示从哪个路径跳转而来，next 表示放行
   // next() 放行  next('/login') 强制跳转
   if (to.path === '/') return next()

   // 获取token
   const tokenStr = window.sessionStorage.getItem('token')
   if (!tokenStr & to.path !== '/') return next('/')
   next()
 })

export default router