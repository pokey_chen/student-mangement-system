import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {Toast} from 'vant'
import { MessageBox } from 'element-ui'
// 导入组件
import './plugins/element.js'

// 导入全局样式表
import './assets/css/global.css'
//密码加密
import md5 from 'js-md5'
Vue.prototype.$md5 = md5;
// // 导入富文本编辑器
 import VueQuillEditor from 'vue-quill-editor'
//  导入对应的样式
 import 'quill/dist/quill.core.css'
 import 'quill/dist/quill.snow.css'
 import 'quill/dist/quill.bubble.css'

// 导入 NProgress 对应的JS和CSS 进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)

Vue.prototype.$alert = MessageBox.alert

import axios from 'axios'

import {api} from './api/api.js'
Vue.prototype.$api = api

axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? '/api' : window.location.protocol + '//' + window.location.host

// request 拦截器中，展示进度条
axios.interceptors.request.use(config => {
  NProgress.start()
  config.headers.token = window.sessionStorage.getItem('token')

  return config
})

// 设置post请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

// response 拦截其中，隐藏进度条
axios.interceptors.response.use(
  confirm => {
    console.log(confirm.data.retStatus)
    // 服务器状态码不是000的情况
    if (confirm.data.retStatus) {
      switch (confirm.data.retStatus) {
        case '001':
           Toast({
            message: confirm.data.retMsg,
            duration: 1000,
            forbidClick: true
        })
        break
        case '002':
          Toast({
              message: '登录过期，请重新登录',
              duration: 1000,
              forbidClick: true
          })
          // 清除token
          sessionStorage.removeItem('token')
          setTimeout(() => {
            router.replace({
                path: '/',
                query: {
                    redirect: router.currentRoute.fullPath
                }
            });
        }, 1000)
        break
      }
    }
    NProgress.done()
  return confirm
})

Vue.prototype.$http = axios

Vue.config.productionTip = false

Vue.prototype.exportExcel = (Url, FileName) => {
  getdownAjax(Url, {
    responseType: "blob"
  }).then(res => {
    // console.log("res", res);
    if (res) {
      let blob = new Blob([res.data], {
        type: "application/vnd.ms-excel;charset=utf-8"
      }); // res就是接口返回的文件流了
      let objectUrl = URL.createObjectURL(blob);
      // console.log(objectUrl);
      // const fileName = FileName;
      const elink = document.createElement("a");
      elink.download = FileName; //下载文件名称,
      elink.style.display = "none";
      elink.href = objectUrl;
      document.body.appendChild(elink);
      elink.click();
      URL.revokeObjectURL(elink.href); // 释放URL 对象
      document.body.removeChild(elink);
    }
  });
}

Vue.filter('dataFormat', function (originVal) {
  const dt = new Date(originVal)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')
  const h = (dt.getHours() + '').padStart(2, '0')
  const min = (dt.getMinutes() + '').padStart(2, '0')
  const sec = (dt.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${h}:${min}:${sec}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
